# WEBGL

### Articles

1. **[Generative Design]:** Patrik Hübner https://www.edenspiekermann.com/magazine/generative-design-q-and-a-with-creative-algorithm-developer-patrik-huebner/
2. **[WebGL]:** Write a Toon Shader in Unity <https://roystan.net/articles/toon-shader.html>
3. **[Webgl]:** Case study The Frontier Within, Active Theory https://medium.com/active-theory/thorne-the-frontier-within-7c97f8a49719



### Tutorials

1. **[GLSL Tutorials]**: http://pixelshaders.com
2. **[GLSL Tutorials]**: http://hughsk.io/fragment-foundry/chapters/01-hello-world
3. **[Generative art]:** https://generativeartistry.com
4. **[THREEjs & shaders material treatments]**: <https://doublejump.github.io/webgl-workshop/guide/index.html>
5. **[SHADERS]**: Pseudo lens flare  <http://john-chapman-graphics.blogspot.com/2013/02/pseudo-lens-flare.html?m=1>
6. **[Intermediate tutorials]:** some useful tutorial on intermediate examples <https://blog.mozvr.com/threejs-intermediate-skill-tutorials/>





### Useful

1. **[WebGl]**: Random examples https://webgl-shaders.com
2. **[Toon shader]:** https://github.com/mnmxmx/toon-shading
3. **[THREEJs**: tips & tricks <https://discoverthreejs.com/tips-and-tricks/>
4. **[Shaders & RayMarching]:** <http://jamie-wong.com/2016/07/15/ray-marching-signed-distance-functions/>
5. **[How bezier works]:** <http://jamie-wong.com/post/bezier-curves/>
6. **[Metaballs]:** <http://jamie-wong.com/2016/07/06/metaballs-and-webgl/>
7. **[SDF Functions - IMPORTANT]**: <http://iquilezles.org/www/articles/distfunctions/distfunctions.htm>
8. **[Fake 3D from 2D]**: Fake 3d photo in pixi using a depth map and a diplacement filter <https://www.arpatech.com/blog/3d-parallax-effect-2d-images-depth-map/>
9. **[Couch Game]:** Unity twitter channel on how to use shaders to create stuff <https://twitter.com/CouchGameCraft>
10. **[Shaders library]:** useful archive of differents shaders for different usage <https://www.geeks3d.com/shader-library/>
11. **[RayMarching - SDF]:** Basic for shader showdown <https://www.geeks3d.com/20130524/building-worlds-with-distance-functions-in-glsl-raymarching-glslhacker-tutorial-opengl/>
12. **[SDF]:** Introduction to Signed Distance Field <https://www.alanzucconi.com/tag/signed-distance-field/>
13. **[DOG]:** Discussion on the doggo of dog studio https://twitter.com/kchplr/status/1113161040265666560?s=21 
14. **[STONEWALL FOREVER]:** Case study 



### Video Channels

1. **[Art of code]**: https://www.youtube.com/channel/UCcAlTqd9zID6aNX3TzwxJXg
2. **[Evvvil]:** Shader showdown creations - deconstructions https://www.twitch.tv/evvvvil_
3. **[Greg Tatum]**: Shaders & webgl <https://www.youtube.com/channel/UC0PZFacfD3raYE6OhF0_mFw>
4. **[Jorge Rodriguez]:**  Rendering & shaders <https://www.youtube.com/playlist?list=PLW3Zl3wyJwWMpFSRpeMmSBGDShbkiV1Cq>



### Stand-alone videos

1. **[Generative design talk]:** A talk by Patrik Hübner https://www.youtube.com/watch?v=-ntgKfXFbSk&feature=youtu.be&t=82



